import {Resolver, Args, Query} from "@nestjs/graphql";
import {VehicleService} from "../services/vehicle.service";
import {Vehicle} from "../schemas/vehicle.schema";
import {FiltersBddInput} from "../inputs/filters-bdd.input";


@Resolver(of => Vehicle)
export class VehicleResolver {

  constructor(
    private vehicleService: VehicleService
  ) {}

  @Query(returns => [Vehicle], {nullable: true})
  async getAllVehiclesFromApi(
    @Args('filters', {nullable: true}) filters: FiltersBddInput
  ): Promise<Vehicle[] | void> {
    const options = filters ? filters : { skip: 0, limit: 50 };

    return await this.vehicleService.handleMakes() ? await this.vehicleService.handleMakeTypes(options) : [];
  }
}
