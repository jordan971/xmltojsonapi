import { VehicleResolver } from "./vehicle.resolver";
import { VehicleService } from '../services/vehicle.service';
import {Test} from "@nestjs/testing";
import {HttpModule, HttpService} from "@nestjs/axios";
import {FetchDataService} from "../services/fetch-data.service";
import {closeInMongodConnection, rootMongooseTestModule} from "../../../test/utils/mongoose/mongoose-test.module";
import {MongooseModule} from "@nestjs/mongoose";
import {Vehicle} from "../schemas/vehicle.schema";
import {VehicleSchema} from "../schemas/vehicle.schema";
import {VehicleRepository} from "../repositories/vehicle.repository";

describe('VehiculeResolver', () => {
  let vehicleResolver: VehicleResolver;
  let vehicleService: VehicleService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers : [
        VehicleResolver,
        VehicleService,
        FetchDataService,
        VehicleRepository
      ],
      imports: [
        rootMongooseTestModule(),
        MongooseModule.forFeature([{ name: Vehicle.name, schema: VehicleSchema }]),
        HttpModule
      ]
    }).compile();

    vehicleService = moduleRef.get<VehicleService>(VehicleService);
    vehicleResolver = moduleRef.get<VehicleResolver>(VehicleResolver);
  });

  describe('vehiculeResolver', () => {
    it('should be defined', async () => {
      expect(vehicleResolver).toBeDefined();
      expect(vehicleService).toBeDefined();
    });
  });

  describe('getAllVehiclesFromApi', () => {
    it('should return response', async () => {
      vehicleService.handleMakes = jest.fn().mockReturnValueOnce(makesWithoutMakeTypes);
      vehicleService.handleMakeTypes = jest.fn().mockReturnValueOnce(makesWithMakeTypes);

      expect(await vehicleResolver.getAllVehiclesFromApi({skip: 0, limit: 50})).toStrictEqual(makesWithMakeTypes);
      expect(vehicleService.handleMakeTypes).toBeCalledWith({skip: 0, limit: 50});
      expect(vehicleService.handleMakes).toBeCalledTimes(1);
    });
  });

  afterAll(() => {
    closeInMongodConnection()
  })
});

const makesWithoutMakeTypes = [
  {
    "makeId": "11897",
    "makeName": " MID-TOWN TRAILERS",
  },
  {
    "makeId": "4877",
    "makeName": "1/OFF KUSTOMS, LLC",
  },
  {
    "makeId": "9172",
    "makeName": "1M CUSTOM CAR TRANSPORTS, INC.",
  }
];

const makesWithMakeTypes = [
  {
    "makeId": "11897",
    "makeName": " MID-TOWN TRAILERS",
    "vehicleTypes": [
      {
        "typeId": "6",
        "typeName": "Trailer"
      }
    ]
  },
  {
    "makeId": "4877",
    "makeName": "1/OFF KUSTOMS, LLC",
    "vehicleTypes": [
      {
        "typeId": "1",
        "typeName": "Motorcycle"
      }
    ]
  },
  {
    "makeId": "9172",
    "makeName": "1M CUSTOM CAR TRANSPORTS, INC.",
    "vehicleTypes": [
      {
        "typeId": "6",
        "typeName": "Trailer"
      }
    ]
  }
];
