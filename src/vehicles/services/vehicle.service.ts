import {ForbiddenException, Injectable} from "@nestjs/common";
import {HttpService} from "@nestjs/axios";
import {Vehicle} from '../schemas/vehicle.schema';
import {FetchDataService} from "./fetch-data.service";
import {parseString} from 'xml2js';
import {VehicleRepository} from "../repositories/vehicle.repository";

@Injectable()
export class VehicleService {
  constructor(
    private readonly httpService: HttpService,
    private fetchDataService: FetchDataService,
    private vehicleRepository: VehicleRepository
  ) {}

  async handleMakes(): Promise<any> {
    await this.vehicleRepository.deleteMany();
    const url = 'https://vpic.nhtsa.dot.gov/api/vehicles/getallmakes?format=XML';
    const fetchedMakes = await this.fetchDataService.fetchDataFromWebservice(url);
    const transformedDataXmlToJson: any = await this.transformDataXmlToJson(fetchedMakes);
    const transformedMakes = this.transformMakesData(transformedDataXmlToJson["AllVehicleMakes"]);
    await this.vehicleRepository.create(transformedMakes);

    return true;
  }

  transformDataXmlToJson(vehiculeMakes) {

    return new Promise((resolve, reject) => {
      parseString(vehiculeMakes.data, {explicitArray : false}, async (err, result) => {
        if (err) {
          reject(err);
        }

        return resolve(result["Response"]["Results"]);
      })
    }).catch((err) => {

      throw new ForbiddenException(err);
    });
  }

  transformMakesData(data): {makeId: string; makeName: string}[] {
    const results: {makeId: string; makeName: string}[] = [];
    data.map(async (data) => {
      results.push({
        makeId: data["Make_ID"],
        makeName: data["Make_Name"],
      });
    });

    return results;
  }

  async handleMakeTypes(filters): Promise<any> {
    const makes = await this.findMakesFromBdd(filters);
    await this.fetchMakeTypes(makes);

    return this.findMakesFromBdd(filters);
  }

  private async fetchMakeTypes(makes: Vehicle[]) {
    for (let make of makes) {
      const url = `https://vpic.nhtsa.dot.gov/api/vehicles/GetVehicleTypesForMakeId/${make.makeId}?format=xml`;
      const fetchedMakeTypes = await this.fetchDataService.fetchDataFromWebservice(url);
      const transformedDataXmlToJson: any = await this.transformDataXmlToJson(fetchedMakeTypes);
      let transformedDataAttributes = this.transformMakeTypesData(transformedDataXmlToJson["VehicleTypesForMakeIds"]);

      await this.vehicleRepository.updateOne({makeId: make.makeId},
        {
          $set: {
            vehicleTypes: transformedDataAttributes
          }
        });
    }
  }

  transformMakeTypesData(data) {
    const results: {typeId: string; typeName: string}[] = [];
    if (Array.isArray(data)) {
      data.map(async (data) => {
        results.push({
          typeId: data["VehicleTypeId"],
          typeName: data["VehicleTypeName"]
        });
      });
    } else {
      if (data["VehicleTypeId"] !== undefined) {
        results.push({
          typeId: data["VehicleTypeId"],
          typeName: data["VehicleTypeName"]
        });
      }
    }

    return results;
  }

  private findMakesFromBdd(options) {

    return this.vehicleRepository.find({},{}, options);
  }
}
