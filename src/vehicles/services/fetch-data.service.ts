import {ForbiddenException, Injectable} from "@nestjs/common";
import {HttpService} from "@nestjs/axios";
import {parseString} from 'xml2js';

@Injectable()
export class FetchDataService {
  constructor(
    private readonly httpService: HttpService
  ) {}

  async fetchDataFromWebservice(url: string): Promise<any> {

    return await this.httpService.axiosRef.get(url)
  }
}
