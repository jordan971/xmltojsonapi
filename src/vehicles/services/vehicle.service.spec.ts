import { VehicleService } from './vehicle.service';
import {Test, TestingModule} from "@nestjs/testing";
import {Vehicle} from "../schemas/vehicle.schema";
import {FetchDataService} from "./fetch-data.service";
import {getModelToken} from "@nestjs/mongoose";
import {HttpModule} from "@nestjs/axios";
import {VehicleRepository} from "../repositories/vehicle.repository";

describe('VehicleService', () => {
  let vehicleService: VehicleService;
  let fetchDataService: FetchDataService;

  beforeEach(async () => {
    function mockVehicleRepository(dto: any = null) {
      this.data = dto;
      this.create = () => {
        return makesWithoutMakeTypes;
      };
    }
    const moduleRef: TestingModule = await Test.createTestingModule({
      providers : [
        VehicleService,
        FetchDataService,
        VehicleRepository,
        {
          provide: getModelToken(Vehicle.name),
          useValue: mockVehicleRepository
        }
      ],
      imports: [
        HttpModule
      ]
    }).compile();

    vehicleService = moduleRef.get<VehicleService>(VehicleService);
    fetchDataService = moduleRef.get<FetchDataService>(FetchDataService);
  });

  describe('transformMakesData', () => {
    it('should transform data into proper json to get stored in bdd', async () => {
      const data = [
        { Make_ID: '11897', Make_Name: ' MID-TOWN TRAILERS' },
        { Make_ID: '4877', Make_Name: '1/OFF KUSTOMS, LLC' },
        { Make_ID: '9172', Make_Name: '1M CUSTOM CAR TRANSPORTS, INC.' },
      ];

      expect(vehicleService.transformMakesData(data)).toStrictEqual(makesWithoutMakeTypes);
    });
  });

  describe('transformMakeTypesData', () => {
    it('should transform data array of objects into proper json to get stored in bdd', async () => {
      const data = [
        { VehicleTypeId: '6', VehicleTypeName: 'Trailer' }
      ]

      expect(vehicleService.transformMakeTypesData(data)).toStrictEqual([
        { typeId: '6', typeName: 'Trailer' }
      ]);
    });

    it('should transform data object into proper json to get stored in bdd', async () => {
      const data = { VehicleTypeId: '6', VehicleTypeName: 'Trailer' }

      expect(vehicleService.transformMakeTypesData(data)).toStrictEqual([
        { typeId: '6', typeName: 'Trailer' }
      ]);
    });
  });

  describe('transformDataXmlToJson', () => {
    it('should transform xml data into json', async () => {
      const data = {
        AllVehicleMakes: [
          {Make_ID: '11897', Make_Name: ' MID-TOWN TRAILERS'},
          {Make_ID: '4877', Make_Name: '1/OFF KUSTOMS, LLC'},
          {Make_ID: '11257', Make_Name: '102 IRONWORKS, INC.'}
        ]
      };

      await expect(vehicleService.transformDataXmlToJson(xmlData)).resolves.toStrictEqual(data);
    });
  });
});

const makesWithoutMakeTypes = [
  {
    "makeId": "11897",
    "makeName": " MID-TOWN TRAILERS",
  },
  {
    "makeId": "4877",
    "makeName": "1/OFF KUSTOMS, LLC",
  },
  {
    "makeId": "9172",
    "makeName": "1M CUSTOM CAR TRANSPORTS, INC.",
  }
];

const xmlData = {
  data: `<Response xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <Count>10432</Count>
    <Message>Response returned successfully</Message>
    <Results>
      <AllVehicleMakes>
        <Make_ID>11897</Make_ID>
        <Make_Name> MID-TOWN TRAILERS</Make_Name>
      </AllVehicleMakes>
      <AllVehicleMakes>
        <Make_ID>4877</Make_ID>
        <Make_Name>1/OFF KUSTOMS, LLC</Make_Name>
      </AllVehicleMakes>
      <AllVehicleMakes>
        <Make_ID>11257</Make_ID>
        <Make_Name>102 IRONWORKS, INC.</Make_Name>
      </AllVehicleMakes>
    </Results>
  </Response>`
};

