import { VehicleService } from './vehicle.service';
import {Test, TestingModule} from "@nestjs/testing";
import {FetchDataService} from "./fetch-data.service";
import {HttpService} from "@nestjs/axios";
import {of} from "rxjs";

describe('VehicleService', () => {
  let httpService: HttpService;
  let fetchDataService: FetchDataService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      providers : [
        FetchDataService,
        {
          provide: HttpService,
          useValue: {
            axiosRef: {
              get: jest.fn().mockImplementation(() => xmlData)
            }
          }
        }
      ]
    }).compile();

    fetchDataService = moduleRef.get<FetchDataService>(FetchDataService);
    httpService = moduleRef.get<HttpService>(HttpService);
  });

  describe('fetchDataService', () => {
    it('should be defined and return xml response', async () => {

      expect(fetchDataService).toBeDefined();
      expect(
        await fetchDataService
          .fetchDataFromWebservice('http://localhost/api/getMakes?format=XML'))
          .toStrictEqual(xmlData);
    });
  });
});

const xmlData = {
  data: `<Response xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <Count>10432</Count>
    <Message>Response returned successfully</Message>
    <Results>
      <AllVehicleMakes>
        <Make_ID>11897</Make_ID>
        <Make_Name> MID-TOWN TRAILERS</Make_Name>
      </AllVehicleMakes>
      <AllVehicleMakes>
        <Make_ID>4877</Make_ID>
        <Make_Name>1/OFF KUSTOMS, LLC</Make_Name>
      </AllVehicleMakes>
      <AllVehicleMakes>
        <Make_ID>11257</Make_ID>
        <Make_Name>102 IRONWORKS, INC.</Make_Name>
      </AllVehicleMakes>
    </Results>
  </Response>`
};


