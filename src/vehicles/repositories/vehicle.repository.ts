import {Vehicle, VehicleDocument} from '../schemas/vehicle.schema';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";

export class VehicleRepository {
  constructor(
    @InjectModel(Vehicle.name) private readonly vehicleModel: Model<VehicleDocument>
  ) {}

  create(makes) {

    return this.vehicleModel.create(makes);
  }

  updateOne(filter, update) {

    return this.vehicleModel.updateOne(filter, update);
  }

  find(filter, projection, options) {

    return this.vehicleModel.find(filter, projection, options);
  }

  deleteMany(filter = {}) {

    return this.vehicleModel.deleteMany(filter);
  }
}
