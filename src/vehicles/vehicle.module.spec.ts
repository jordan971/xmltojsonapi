import { Test } from '@nestjs/testing';
import {VehicleModule} from "./vehicle.module";
import {MongooseModule} from "@nestjs/mongoose";
import {closeInMongodConnection, rootMongooseTestModule} from "../../test/utils/mongoose/mongoose-test.module";
import {Vehicle, VehicleSchema} from "./schemas/vehicle.schema";

describe('VehicleModule', () => {
  beforeAll(done => {
    done();
  });
  it('should be defined', async () => {
    const vehicleModule = await Test.createTestingModule({
      imports: [
        VehicleModule,
        rootMongooseTestModule(),
        MongooseModule.forFeature([{ name: Vehicle.name, schema: VehicleSchema }]),
      ],
    }).compile();

    expect(vehicleModule).toBeDefined();
  });

  afterAll(done => {
    closeInMongodConnection();
    done();
  });
});
