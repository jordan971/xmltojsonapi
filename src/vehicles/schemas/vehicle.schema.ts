import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory, raw } from '@nestjs/mongoose';
import {Field, ObjectType} from '@nestjs/graphql';

export type VehicleDocument = Vehicle & Document;

@Schema()
@ObjectType()
class VehicleType {
  @Prop()
  @Field()
  typeId: string;

  @Prop()
  @Field()
  typeName: string;
}

export const VehicleTypeSchema = SchemaFactory.createForClass(VehicleType);

@Schema()
@ObjectType()
export class Vehicle {
  @Prop()
  @Field()
  makeId: string;

  @Prop()
  @Field()
  makeName: string;

  @Prop([VehicleTypeSchema])
  @Field(type => [VehicleType], {nullable: true})
  vehicleTypes: VehicleType[];
}

export const VehicleSchema = SchemaFactory.createForClass(Vehicle);
