import {Field, InputType} from '@nestjs/graphql';

@InputType()
export abstract class FiltersBddInput {
  @Field()
  skip: number;

  @Field()
  limit: number;
}
