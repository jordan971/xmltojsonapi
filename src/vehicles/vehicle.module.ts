import { Module } from '@nestjs/common';
import { VehicleService } from "./services/vehicle.service";
import { VehicleResolver } from "./resolvers/vehicle.resolver";
import {HttpModule} from "@nestjs/axios";
import {FetchDataService} from "./services/fetch-data.service";
import {MongooseModule} from "@nestjs/mongoose";
import { VehicleSchema, Vehicle } from "./schemas/vehicle.schema";
import {VehicleRepository} from "./repositories/vehicle.repository";

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([
      { name: Vehicle.name, schema: VehicleSchema }
    ])
  ],
  providers: [
    FetchDataService,
    VehicleService,
    VehicleResolver,
    VehicleRepository
  ]
})
export class VehicleModule {}
