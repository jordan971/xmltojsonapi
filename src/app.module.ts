import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { VehicleModule } from './vehicles/vehicle.module';
import { ConfigModule } from "@nestjs/config";
import { ApolloDriver } from '@nestjs/apollo';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    GraphQLModule.forRoot({
      driver: ApolloDriver,
      installSubscriptionHandlers: true,
      autoSchemaFile: './src/schema.gql'
    }),
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    MongooseModule.forRoot(String(process.env.DATABASE_URL)),
    VehicleModule
  ]
})
export class AppModule {}
